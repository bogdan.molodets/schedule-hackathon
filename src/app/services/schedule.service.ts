import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { HelperService } from './helper.service';
import 'rxjs/add/operator/map';

@Injectable()
export class ScheduleService {

  constructor(private httpClient: HttpClient, private helperService: HelperService) {
    }
    getGroupList():Promise<any>{
      return this.httpClient.get(`${environment.apiUrl}`).pipe(catchError(this.helperService.handleError('getListByGroupId', []))).toPromise();
    }

    getScheduleByGroupId(id: number): Promise<any>{
      return this.httpClient.get(`${environment.apiUrl}`).pipe(catchError(this.helperService.handleError('getScheduleByGroupId', []))).toPromise();
    }

    getDetailByTeacherId(id: number): Promise<any> {
      return this.httpClient.get(`${environment.apiUrl}`).pipe(catchError(this.helperService.handleError('getDetailByTeacherId', []))).toPromise();
    }
    
}
