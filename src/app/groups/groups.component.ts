import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  @Output() selectGroupEmitter: EventEmitter<any> = new EventEmitter()
  groups = [{id:1, classNumber: '11-A'},{id:2, classNumber: '11-A'},{id:2, classNumber: '11-A'},{id:2, classNumber: '11-A'},{id:2, classNumber: '11-A'},{id:2, classNumber: '11-A'},{id:2, classNumber: '11-A'}]
  constructor() { }

  ngOnInit() {
  }
  selectGroup(group){
    this.selectGroupEmitter.emit(group);
  }
}
