import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { GroupsComponent } from './groups/groups.component';
import { TableComponent } from './table/table.component';

import { ScheduleService } from './services/schedule.service';
import { HelperService } from './services/helper.service';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ScheduleComponent,
    GroupsComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [ScheduleService, HelperService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
